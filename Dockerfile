FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /django
COPY ./app/requirements.txt /django/
RUN pip install -r requirements.txt
COPY ./app /django